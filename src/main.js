// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import router from './router'
import axios from 'axios'
import VueCookies from 'vue-cookies'
import md5 from 'js-md5';

Vue.use(VueCookies)

Vue.config.productionTip = false
Vue.use(ElementUI)

Vue.prototype.$getHost = function() {
	return 'http://localhost:8080'
}
Vue.prototype.$message.error = function(msg) {
	return Vue.prototype.$message({
		showClose: true,
    	message: msg,
    	type: 'error'
	})
}
Vue.prototype.$message.success = function(msg) {
	return Vue.prototype.$message({
		showClose: true,
    	message: msg,
    	type: 'success'
	})
}
Vue.prototype.md5 = md5
Vue.prototype.$generateCookie = function(username) {
	this.$cookies.config('1h')
  	this.$cookies.set('username',this.user.username + '-' + this.md5(username+'SERECTKEY'))
}
Vue.prototype.$checkCookie = function() {
	let value = this.$cookies.get('username')
	if (value == null)
		return false
	let username = value.split('-')[0]
	let hashcode = value.split('-')[1]
	if(username == '' || hashcode == '' || this.md5(username + 'SERECTKEY') != hashcode)
		return false
	else 
		return true
}
Vue.prototype.$getCookie = function() {
	return this.$cookies.get('username').split('-')[0]
}

Vue.prototype.$ajax = axios

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})

