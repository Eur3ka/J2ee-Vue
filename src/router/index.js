import Vue from 'vue'
import Router from 'vue-router'
import LogIn from '@/components/LogIn'
import Register from '@/components/Register'
import Navigator from '@/components/Navigator'
import Home from '@/components/Home'
import ReservationInfo from '@/components/ReservationInfo'
import Reservation from '@/components/Reservation'
import Admin from '@/components/Admin'
//import reserveResult from '@/components/reserveResult'
import categoryList from '@/components/categoryList'


Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Navigator',
      component: Navigator,
      children: [
        {
          path: '/home',
          name: 'Home',
          component: Home
        },
        {
          path: '/myReservationInfo',
          name: 'ReservationInfo',
          component: ReservationInfo
        },
        {
          path: '/reservation/:rcategoryid',
          name: 'Reservation',
          component: Reservation
        },
        // {
        //   path: '/reserveResult/:msg',
        //   name: 'reserveResult',
        //   component: reserveResult
        // },
        {
          path:'/categoryList',
          name:'categoryList',
          component:categoryList
        },
        {
          path: '/admin',
          name: 'Admin',
          component: Admin
        }
      ]
    },
    {
      path: '/logIn',
      name: 'LogIn',
      component: LogIn
    },
    {
      path: '/register',
      name: 'Register',
      component: Register
    } 
  ]
})
